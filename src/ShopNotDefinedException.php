<?php

namespace Drupal\lightspeed_ecom;

/**
 * Exception thrown when attempting to use a undefined shop.
 *
 * @package Drupal\lightspeed_ecom
 */
class ShopNotDefinedException extends \RuntimeException {

}
