<?php

namespace Drupal\lightspeed_ecom;

/**
 * Exception thrown when attempting to use a disabled shop.
 *
 * @package Drupal\lightspeed_ecom
 */
class ShopDisabledException extends \RuntimeException {

}
